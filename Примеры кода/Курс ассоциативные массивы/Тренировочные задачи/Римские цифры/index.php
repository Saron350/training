<!--еализуйте функцию toRoman(), которая переводит арабские числа в римские.
 Функция принимает на вход целое число в диапазоне от 1 до 3000,
 а возвращает строку с римским представлением этого числа.-->
<!---->
<!--Реализуйте функцию toArabic(),
 которая переводит число в римской записи в число, записанное арабскими цифрами.
  Если переданное римское число не корректно, то функция должна вернуть значение false.-->
<?php
function toRoman($num) {
    $romanNumerals = array(
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1
    );

    $result = '';
    foreach ($romanNumerals as $symbol => $value) {
        while ($num >= $value) {
            $result .= $symbol;
            $num -= $value;
        }
    }

    return $result;
}

function toArabic($roman) {
    $romanNumerals = array(
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1
    );

    $result = 0;
    $prevValue = 0;

    for ($i = 0; $i < strlen($roman); $i++) {
        $symbol = $roman[$i];
        $value = $romanNumerals[$symbol] ?? null;

        if ($value === null) {
            return false;
        }

        if ($value > $prevValue) {
            $result += $value - 2 * $prevValue;
        } else {
            $result += $value;
        }

        $prevValue = $value;
    }

    return $result;
}
echo toRoman(1984);
echo toArabic('MCMLXXXIV');