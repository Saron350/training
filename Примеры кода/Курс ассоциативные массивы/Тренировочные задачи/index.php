<!--Функция, которая принимает на вход путь до файла и возвращает информацию об этом файле в виде ассоциативного массива.-->
<?php
function getFileInfo($filepath)
    {
    $pathParts = explode('/', $filepath);
    $filename = $pathParts[array_key_last($pathParts)];
    $nameParts = explode('.', $filename);
    $extension = $nameParts[array_key_last($nameParts)];

    // В значения вместо переменных подставятся нужные значения
    $info = ['filename' => $filename, 'extension' => $extension];

    return $info;
    } ?>
<!--второй вариант например если нужно добавлять по условию-->
<?php
function getFileInfo2($filepath)
{
// Инициализация массива
$info = [];

$pathParts = explode('/', $filepath);
$filename = $pathParts[array_key_last($pathParts)];
$info['filename'] = $filename;

$nameParts = explode('.', $filename);
$extension = $nameParts[array_key_last($nameParts)];
$info['extension'] = $extension;

return $info;
}
?>


<!--Реализуйте функцию getDomainInfo(), которая принимает на вход имя сайта и возвращает информацию о нем:-->

<?php
//
//use function App\Domains\getDomainInfo;
//
//// Если домен передан без указания протокола,
//// то по умолчанию берется http
//getDomainInfo('yandex.ru');
//// [
////     'scheme' => 'http',
////     'name' => 'yandex.ru'
//// ]
function getDomainInfo(string $url): array
{
    $result = [];
    $inform = explode('://', $url);
    if ($inform[0] == 'http' || $inform[0] == 'https') {
        $result['scheme'] = $inform[0];
        $result['name'] = $inform[1];
    } else {
        $result['scheme'] = 'http';
        $result['name'] = $inform[0];
    }
    return $result;
}

//второй вариант

function getDomainInfo2(string $domain): array
{
    if (substr($domain, 0, 8) === 'https://') {
        $scheme = 'https';
    } else {
        $scheme = 'http';
    }

    $name = str_replace("{$scheme}://", '', $domain);

    return ['scheme' => $scheme, 'name' => $name];
}
//Реализуйте функцию normalize(), которая "нормализует" данные переданного урока. То есть приводит их к определенному виду.
//
//На вход этой функции подается ассоциативный массив, описывающий собой урок курса. В уроке содержатся два поля: имя и описание.
// Условия:
// Имя капитализируется. То есть первый символ каждого слова имени становится заглавным, остальные маленькими
// Весь текст описания приводится к нижнему регистру
function normalize(array &$array)
{
    $array['name'] = mb_convert_case($array['name'], MB_CASE_TITLE);
    $array['description'] = mb_strtolower($array['description']);
}

//Реализовать функцию, которая считает колличество одинаковых ключей в массиве
$fruits = [
    'apple', 'banana', 'pear',
    'apricot', 'apple', 'banana',
    'apple', 'orange', 'pear'
];
function countFruits($fruits)
{
    $result = [];

    foreach ($fruits as $name) {
        // Проверка на существование
        if (array_key_exists($name, $result)) {
            $result[$name] += 1;
        } else {
            $result[$name] = 1;
        }
    }
//    или используя оператор объядинения с нулем
    foreach ($fruits as $name) {
        $result[$name] = ($result[$name] ?? 0) + 1;
    }

    return $result;
}
//Реализуйте функцию countWords(),
// которая считает количество слов в предложении и возвращает ассоциативный массив в котором ключи это слова (приведенные к нижнему регистру),
//  а значения — это то сколько раз слово встретилось в предложении.
//   Слова в предложении могут находиться в разных регистрах.
// Перед подсчетом их нужно приводить в нижний регистр, чтобы не пропускались дубли.
function countWords(string $string): array
{
    if($string == ''){
        return [];
    }
    $result = [];
    $explodeString = explode(' ', mb_strtolower($string));
    foreach ($explodeString as $item) {
        $result[$item] = ($result[$item] ?? 0) + 1;
    }
    return $result;
}
