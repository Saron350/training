<?php
//Тема урока юникод, отладка, виды ошибок
//Реализовать функцию которая проверяет передаваемое слово на полиндром, при этом запрещено использовать strrev();
//важно, функция должна отрабатывать и с киррилицей
namespace App\Text;

// BEGIN (write your solution here)
function isPalindrome(string $string):bool
{
    $char = mb_strlen($string);
    $char2 = mb_strlen($string);
    $result = '';
    $result2 = mb_substr($string, 0,$char);
    for ($j = $char2; $j >= 0; $j--) {
        $result .= mb_substr($string, $j, 1);
    }
    if ($result == $result2) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
print_r(isPalindrome('довод'));
// END

//другой вариант
namespace App\Text;

// BEGIN
function isPalindrome(string $word)
{
    $lastIndex = mb_strlen($word) - 1;
    $middleIndex = $lastIndex / 2;
    for ($i = 0; $i < $middleIndex; $i++) {
        $symbol = mb_substr($word, $i, 1);
        $mirroredSymbol = mb_substr($word, $lastIndex - $i, 1);
        if ($symbol !== $mirroredSymbol) {
            return false;
        }
    }
    return true;
}
// END

//отдельная функция для инверсии через while
function reverse($str)
{
    $index = strlen($str) - 1;
    $reversedString = '';

    while ($index >= 0) {
        $currentChar = $str[$index];
        $reversedString = "{$reversedString}{$currentChar}";
        $index = $index - 1;
    }

    return $reversedString;
}

// phpcs:disable PSR1.Files.SideEffects

// BEGIN (write your solution here)
function isPalindrome(string $string):bool
{
    $strlen = mb_strlen($string);
    $result = mb_substr($string, 0, $strlen);
    require_once 'Strings.php'; //подключаю единожды
    $resultReverse = reverse($string);
    if ($result === $resultReverse) {
        return true;
    } else {
        return false;
    }
}
// END

//пример с использование пространства имен учебного сервера


// phpcs:disable PSR1.Files.SideEffects

namespace Solution;

require_once "Strings.php";
// BEGIN (write your solution here)
function isPalindrome(string $string): bool
{
    $stringlen = mb_strlen($string);
    $result = mb_substr($string, 0, $stringlen);
    $result2 = \Strings\reverse($string);
    if ($result === $result2) {
        return true;
    }
    return false;
}
// END