<?php

namespace CarFactory;

use MachineShop\MachineShop;

class CarFactory extends MachineShop {
    // свойства и методы специфичные для CarFactory
    protected $carModels = [];

    public function addCarModel($carModel) {
        $this->carModels[] = $carModel;
    }

    public function produce() {
        // реализация метода для CarFactory
        echo "Производство автомобилей на фабрике " . $this->name . "\n";
    }
}