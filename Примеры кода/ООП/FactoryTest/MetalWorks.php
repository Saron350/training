<?php

namespace MetalWorks;

use MachineShop\MachineShop;

class MetalWorks extends MachineShop {
    // свойства и методы специфичные для MetalWorks
    protected $metalTypes = [];

    public function addMetalType($metalType) {
        $this->metalTypes[] = $metalType;
    }

    public function produce() {
        // реализация метода для MetalWorks
        echo "Производство металлоконструкций на заводе " . $this->name . "\n";
    }
}