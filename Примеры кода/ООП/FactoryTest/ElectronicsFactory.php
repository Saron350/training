<?php

namespace ElectronicsFactory;

use MachineShop\MachineShop;

class ElectronicsFactory extends MachineShop {
    // свойства и методы специфичные для ElectronicsFactory
    protected $productTypes = [];

    public function addProductType($productType) {
        $this->productTypes[] = $productType;
    }

    public function produce() {
        // реализация метода для ElectronicsFactory
        echo "Производство электроники на фабрике " . $this->name . "\n";
    }
}