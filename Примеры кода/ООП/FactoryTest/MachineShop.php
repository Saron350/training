<?php

namespace MachineShop;

abstract class MachineShop {
    // общие свойства и методы для всех наследников
    protected $name;
    protected $location;
    protected $employees = [];

    public function __construct($name, $location) {
        $this->name = $name;
        $this->location = $location;
    }

    public function hireEmployee($employee) {
        $this->employees[] = $employee;
    }

    // абстрактный метод, который должны реализовать все наследники
    abstract public function produce();

    // метод, который будут использовать все наследники
    public function getName() {
        return $this->name;
    }
}