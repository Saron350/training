<?php
//Реализуйте функцию getSuperSeriesWinner($scores), которая находит команду победителя для конкретной суперсерии.
// Победитель определяется как команда, у которой больше побед (не количество забитых шайб) в конкретной серии.
// Функция принимает на вход массив, в котором каждый элемент это массив, описывающий счет в конкретной игре (сколько шайб забила Канада и СССР).
// Результат функции – название страны: 'canada', 'ussr'. Если суперсерия закончилась в ничью, то нужно вернуть null.
namespace App\Superseries;

// BEGIN (write your solution here)
function getSuperSeriesWinner($scores)
{
    $canadaWins = 0;
    $ussrWins = 0;
    foreach ($scores as $game) {
        if ($game[0] > $game[1]) {
            $canadaWins++;
        } elseif ($game[0] < $game[1]) {
            $ussrWins++;
        }
    }
    if ($canadaWins > $ussrWins) {
        return 'canada';
    } elseif ($canadaWins < $ussrWins) {
        return 'ussr';
    } else {
        return null;
    }
}
// END<?php
