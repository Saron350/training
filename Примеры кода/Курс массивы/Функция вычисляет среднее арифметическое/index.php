<?php
namespace App\Arrays;
//Реализуйте функцию calculateAverage(), которая высчитывает среднее арифметическое элементов массива.
// BEGIN (write your solution here)
function calculateAverage(array $arr):int|float|null
{
    if (empty($arr)) {
        return null;
    }
    $sum = 0;
    foreach ($arr as $num){
        $sum += $num;
    }
    return $result = $sum / count($arr);
}