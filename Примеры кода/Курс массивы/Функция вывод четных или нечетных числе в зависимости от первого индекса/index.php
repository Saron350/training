<?php


//Реализуйте функцию getSameParity, которая принимает на вход массив чисел и возвращает новый, состоящий из элементов, у которых такая же чётность, как и у первого элемента входного массива.

namespace App\Arrays;

// BEGIN (write your solution here)
function getSameParity(array $arr): array
{
    if (empty($arr)) {
        return [];
    }
    $result = [];
    $firstParity = $arr[0] % 2;
    foreach ($arr as $item) {
        if ($firstParity === $item % 2) {
            $result[] = $item;
        }
    }
    return $result;
}