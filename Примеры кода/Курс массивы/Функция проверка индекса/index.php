<?php
namespace App\Arrays;

// BEGIN (write your solution here)
function get(array $array, int $index, $default = null)
{
    if (array_key_exists($index, $array)) {
        return $array[$index];
    } else {
        return $array = $ages[$index] ?? $default;
    }
}