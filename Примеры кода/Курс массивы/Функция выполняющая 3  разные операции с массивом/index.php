<?php
//Реализуйте функцию apply(), которая применяет указанную операцию к переданному массиву и возвращает новый массив. Всего нужно реализовать три операции:
//
//reset - Сброс массива
//remove - Удаление значения по индексу
//change - Обновление значения по индексу

namespace App\Arrays;

function apply(array $arr, string $operationName, int $index = null, $value = null): array
{
    // BEGIN (write your solution here)
    switch ($operationName) {
        case 'reset':
            return [];
            break;
        case 'remove':
            unset($arr[$index]);
            return $arr;
            break;
        case 'change':
            $arr[$index] = $value;
            return $arr;
            break;
    }
    // END
}