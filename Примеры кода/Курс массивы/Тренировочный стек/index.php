<?php
//Дан многомерный массив (см. его под задачей). С помощью цикла выведите строки в формате 'имя-зарплата'.
$arr = [
    0 => ['name' => 'Коля', 'salary' => 300],
    1 => ['name' => 'Вася', 'salary' => 400],
    2 => ['name' => 'Петя', 'salary' => 500],
];

foreach ($arr as $item) {
    echo $item['name'] . '-' . $item['salary'] . '<br>';
}
//С помощью только одного цикла нарисуйте следующую пирамидку:
//1
//22
//333
//4444
//55555
//666666
//7777777
//88888888
//999999999
for ($i = 1; $i <= 9; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        echo $i;
    }
    echo '<br>';
}
//ну и обратка
for ($i = 9; $i >= 1; $i--) {
    for ($j = 9; $j >= $i; $j--) {
        echo $i;
    }
    echo '<br>';
}
//Дан массив со строками. Запишите в новый массив только те строки, которые начинаются с 'http://'.
$arr = ['http://example.com', 'https://example.com', 'http://example.org', 'ftp://example.com'];
$result = [];

foreach ($arr as $str) {
    if (strpos($str, 'http://') === 0) {
        $result[] = $str;
    }
}

print_r($result);
//Создание шахматной доски с помощью цикла for
echo '<table>';

for ($row = 1; $row <= 8; $row++) {
    echo '<tr>';

    for ($col = 1; $col <= 8; $col++) {
        if (($row + $col) % 2 == 0) {
            echo '<td style="background-color: black; height: 25px; width: 25px;"></td>';
        } else {
            echo '<td style="background-color: white; height: 25px; width: 25px;"></td>';
        }
    }

    echo '</tr>';
}

echo '</table>';

//Напишите программу PHP для создания и отображения первых n строк треугольника Флойда. (используйте n = 7 рядов).

$n = 7;
$count = 1; //
for ($i=1; $i<=$n; $i++) {
    for ($j=1; $j<=$i; $j++) {
        echo $count . " ";
        $count++;
    }
    echo "<br>";
}