<?php
namespace App\Arrays;

//Реализуйте функцию getTotalAmount. Функция принимает на вход в виде массива кошелёк с деньгами и название валюты и возвращает сумму денег указанной валюты.
//
//Реализуйте данную функцию используя управляющие инструкции.
//
//Параметры функции:
//
//Массив содержащий купюры различной валюты с различными номиналами
//Наименование валюты

// BEGIN (write your solution here)
function getTotalAmount(array $portMone, string $cur):int|float
{
    $sum = 0;
    foreach ($portMone as $money) {
        $result = explode(' ', $money);
        if ($result[0] == $cur) {
            $sum += $result[1];
        }
        continue;
    }
    return $sum;
}

//или вот так

// BEGIN
function getTotalAmount2(array $money, string $currency): int
{
    $sum = 0;

    foreach ($money as $bill) {
        $currentCurrency = substr($bill, 0, 3);
        if ($currentCurrency !== $currency) {
            continue;
        }
        $denomination = (int) substr($bill, 4);
        $sum += $denomination;
    }

    return $sum;
}
// END