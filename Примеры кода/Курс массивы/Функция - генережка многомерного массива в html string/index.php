<?php

//Реализуйте функцию buildDefinitionList, которая генерирует html список определений (теги dl, dt и dd) и возвращает получившуюся строку.
// При отсутствии элементов в массиве функция возвращает пустую строку.
namespace App\Strings;

// BEGIN (write your solution here)
function buildDefinitionList(array $array): string
{
    if (empty($array)) {
        return '';
    }
    $one = [];

    foreach ($array as $key => $part) {
        $one[] = "<dt>{$part[0]}</dt>";
        $one[] = "<dd>{$part[1]}</dd>";
    }
    $innerValue = implode('', $one);
    return $result = "<dl>{$innerValue}</dl>";
}
// END

//или вот такой вариант

namespace App\Strings;

// BEGIN
function buildDefinitionList2(array $definitions)
{
    if (empty($definitions)) {
        return '';
    }

    $parts = [];
    foreach ($definitions as $definition) {
        $name = $definition[0];
        $description = $definition[1];
        $parts[] = "<dt>{$name}</dt><dd>{$description}</dd>";
    }
    $innerValue = implode('', $parts);
    $result = "<dl>{$innerValue}</dl>";

    return $result;
}